//SERVICE
weatherApp.service('cityService', function(){
    
    this.city = "Penang, Malaysia";
    
});

weatherApp.service('weatherService', ['$resource', function($resource){
    
    this.GetWeather = function(city, days){
        var weatherAPI = $resource("http://api.openweathermap.org/data/2.5/forecast/daily", {
            APPID: "a7181b72fab0b2d0fb0c56480359ecd8",
            callback: "JSON_CALLBACK"
        }, { get: {method:"JSONP"}} );

        return weatherAPI.get({q: city, cnt:days});
        
    };
}]);