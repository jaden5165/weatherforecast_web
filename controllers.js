//CONTROLLER
weatherApp.controller('homeCtrl', ['$scope', '$location', '$resource', 'cityService', function($scope, $location, $resource, cityService){
    
    $scope.city = cityService.city;
    $scope.$watch('city', function(){
        cityService.city = $scope.city;
    });
    $scope.submit = function(){
        $location.path("/forecast");
    };
    
}]);

weatherApp.controller('forecastCtrl',  ['$scope', 'weatherService', '$routeParams', 'cityService', function($scope, weatherService, $routeParams, cityService){
    
    $scope.city = cityService.city;
    $scope.days = $routeParams.days || '7';

    $scope.weatherResult = weatherService.GetWeather($scope.city, $scope.days);
    
    $scope.convertToCelcius = function(kelvin) {
        return Math.round(kelvin - 273.15);
    };
    
    $scope.convertToDate = function(dt){
        return new Date(dt * 1000);
    };
    
    console.log($scope.weatherResult);
}]);

